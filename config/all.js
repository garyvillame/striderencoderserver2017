'use strict';

module.exports = {
  templateEngine: 'swig',
  assets: {
    mainlib: {
      css: [
        'public/vendor/bootstrap/dist/css/bootstrap.min.css',
        // add additional css files from vendors here (installed through bower)
      ],
      js: [
        'public/vendor/jquery/dist/jquery.min.js',
        'public/vendor/angular/angular.min.js',
        'public/vendor/angular-route/angular-route.min.js',
        // add additional js files from vendors here (installed through bower)
        // js files here are the important files needed to be loaded
      ],
      asyncjs: [
        'public/vendor/bootstrap/dist/js/bootstrap.min.js',
        // add additional js files from vendors here (installed through bower)
        // js files here can be delayed in loading
      ]
    },
    css: [
      'public/css/*.css'
    ],
    // appjs: (process.env.NODE_ENV === "production") ?
      // ['public/dist/application.min.js'] : ['public/mainApp/js/**/*.js']
    appjs: ['public/mainApp/js/**/*.js']
  }
};
