'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
  glob = require('glob');

/**
 * Load app configurations
 */
module.exports = _.extend(
  require('./all')
  // require('./all'),
  // require('./env').config()
);

/**
 * Get files by glob patterns
 */
module.exports.getGlobbedFiles = function(globPatterns, removeRoot) {
  // For context switching
  var _this = this;

  // URL paths regex
  var urlRegex = new RegExp('^(?:[a-z]+:)?\/\/', 'i');

  // The output array
  var output = [];

  // If glob pattern is array so we use each pattern in a recursive way, otherwise we use glob 
  if (_.isArray(globPatterns)) {
    globPatterns.forEach(function(globPattern) {
      output = _.union(output, _this.getGlobbedFiles(globPattern, removeRoot));
    });
  } else if (_.isString(globPatterns)) {
    if (urlRegex.test(globPatterns)) {
      output.push(globPatterns);
    } else {
      glob(globPatterns, {
        sync: true
      }, function(err, files) {
        if (removeRoot) {
          files = files.map(function(file) {
            return file.replace(removeRoot, '');
          });
        }
        output = _.union(output, files);
      });
    }
  }
  return output;
};


/**
* Get Main App Vendor JS Files 
*/

module.exports.getMainVendorJSAssets = function() {
  var output = this.getGlobbedFiles(this.assets.mainlib.js, 'public');
  return output;
};

module.exports.getMainVendorJSAsyncAssets = function() {
  var output = this.getGlobbedFiles(this.assets.mainlib.asyncjs, 'public');
  return output;
};

/**
 * Get Main App Vendor CSS Files 
 */
module.exports.getMainVendorCSSAssets = function() {
  var output = this.getGlobbedFiles(this.assets.mainlib.css, 'public');
  return output;
};


/**
 * Get main app javascript files
 */

module.exports.getMainAppJavascriptAssets = function(){
  var output = this.getGlobbedFiles(this.assets.appjs, 'public');
  return output;
};
