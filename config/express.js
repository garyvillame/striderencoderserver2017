// Should require routing file
// var app = express();
// require('../app/routes/api.js')(app);

var express = require("express");
var bodyParser = require('body-parser');
var compression = require('compression');
var cons = require('consolidate');
var path = require('path');
var swig = require('swig');

var app = express();

var assets = require('./assets');

// Setting application local variables
app.locals.appjsFiles = assets.getMainAppJavascriptAssets();
app.locals.appVendorJSFiles = assets.getMainVendorJSAssets();
app.locals.appVendorJSAsyncFiles = assets.getMainVendorJSAsyncAssets();
app.locals.appVendorCSSFiles = assets.getMainVendorCSSAssets();


app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.use(compression());
app.use(express.static(path.resolve('./public')));

// Set swig as the template engine
app.engine('html', cons['swig']);

// Set views path and view engine
app.set('view engine', 'html');
app.set('views', './app/views');  

// Require routing files
require('../app/routes/web.js')(app);
require('../app/routes/api.js')(app);

// Assume 'not found' in the error msgs is a 404. this is somewhat silly, but valid, you can do whatever you like, set properties, use instanceof etc.
app.use(function(err, req, res, next) {
    // If the error object doesn't exists
    if (!err){
        return next();
    } 
    // Log it
    console.error(err.stack);

    // Error page
    res.status(500).render("error500", {error:err});
});
  

// Assume 404 since no middleware responded
app.use(function(req, res) {
    //res.status(404).render("404");
    res.status(404);
    // respond with html page
    if (req.accepts('html')) {
        res.status(404).render('error');
        return;
    }

    // respond with json
    if (req.accepts('json')) {
        res.status(404).send({ error: 'Not found' });
        return;
    }

    // default to plain-text. send()
    res.type('txt').send('Not found');
});

module.exports = app;
