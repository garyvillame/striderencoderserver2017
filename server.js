var mongoose = require('mongoose');
var dbConnectionString = 'mongodb://192.168.1.57:27017/strider_encoder'
mongoose.connect(dbConnectionString);

mongoose.connection.on('disconnected', function (err) {  
    console.error('\x1b[31m', 'Could not connect to MongoDB!');
    console.log('Will not run server.');
    process.exit(0);
}); 

mongoose.connection.on('connected', function (ref) {
    console.log('connected to mongo server.');
    runServer();
});

function runServer(){
    var app = require('./config/express');

    app.listen(3000, function(){
        console.log('Server running on port 3000');    
    });
}
