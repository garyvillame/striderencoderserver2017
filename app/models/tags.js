var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var TagsSchema = new Schema({
    //TagID       : Number
    EPC         : { type: String, unique: true, required: true },
    Bib         : { type: String, required: true },
    EncoderTime : { type: Date, required: true },
    PeakRSSI    : Number,
    EncoderID   : { type: Number, required: true },
    EventID     : { type: Number, required: true },
    CheckedTime : { type: Date, default: null },
    CheckerID   : { type: Number, default: null },
    Comment     : { type: String, default: null }
})

module.exports = mongoose.model('TagsModel', TagsSchema );