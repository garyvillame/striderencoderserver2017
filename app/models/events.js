var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment');

var EventSchema = new Schema({
    EventID     : { type: Number, required: true },
    EventName   : { type: String, unique: true, required: true },
    Pattern     : { type: String, required: true },
    Header      : { type: String, required: true },
    TotalBibs   : Number,
    Completed   : {type: Number, default: 0}
})

autoIncrement.initialize(mongoose.connection);
EventSchema.plugin(autoIncrement.plugin, { model: 'Event', field: 'EventID'});
module.exports = mongoose.model('EventsModel', EventSchema );