var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment'),
    passportLocalMongoose = require('passport-local-mongoose');


var EncodersSchema = new Schema({
    EncoderID   : { type: Number, required: true },
    Name        : { type: String, unique: true, required: true },
    Login       : { type: String, required: true },
    Password    : { type: String, required: true }
});

autoIncrement.initialize(mongoose.connection);
EncodersSchema.plugin(autoIncrement.plugin, { model: 'Encoders', field: 'EncoderID'});
EncodersSchema.plugin(passportLocalMongoose);
module.exports = mongoose.model('EncodersModel', EncodersSchema );