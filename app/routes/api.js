var EventController = require('../controllers/EventController');
var UserController = require('../controllers/UserController');
var RecordsController = require('../controllers/RecordsController');
var AuthController = require('../controllers/AuthController');



module.exports = function(app){

	app.set('json spaces', 2);
	app.get('/api/getEvents', EventController.getEvents);
	app.get('/api/getCheckerNames', UserController.getCheckerNames);
	app.get('/api/viewRecordsPerEvent/:eventID', RecordsController.viewRecordsPerEvent);
	app.get('/api/getEventByEventID/:eventID', EventController.getEventByEventID);
	
	app.post('/api/submitEncoded', RecordsController.submitEncoded);
	app.post('/api/submitChecked', RecordsController.submitChecked);
	app.post('/api/addEvent', EventController.addEvent);
	app.post('/api/editEvent', EventController.editEvent);
	app.post('/api/addEncoder', UserController.addEncoder);

	// app.get('/api/login', AuthController.userLogin);
};

// EventController, UserController, RecordsController should come from the controllers folder

/* Sample JSON Body for app.post('/api/submitEncoded')
{
	"EPC" : "00001001",
	"Time" : "2017-06-15T12:00:00.000Z",
	"RSSI" : "-50",
	"Checker" : "44",
	"EventID" : "378",
	"Bib" : "12345"
}
*/

/* Sample JSON Body for app.post('/api/submitChecked')
{
	"EPC" : "00001001",
	"Time" : "2017-06-15T13:00:00.000Z",
	"RSSI" : "-50",
	"Checker" : "44",
	"EventID" : "378",
	"Bib" : "12345"
}
*/

/* Sample JSON Body for app.post('/api/addEvent')
{
	"eventData" : {
		"EventName" : "Test Event",
		"Header" : "00",
		"Pattern" : "HH00GGGG",
		"Completed" : 0,
		"TotalBibs" : 1000
	}
}
*/

/* Sample JSON Body for app.post('/api/editEvent')
{
	"EventDetails" : {
		"EventID" : 378,
		"EventName" : "Test Event",
		"Header" : "00",
		"Pattern" : "HH00GGGG",
		"Completed" : 0,
		"TotalBibs" : 1000
	}
}
*/

/* Sample JSON Body for app.post('/api/addEncoder')
{
	"encoderData" : {
		"Name" : "Test User",
		"Login" : "test",
		"Password" : "testpassword"
	}
}
*/
