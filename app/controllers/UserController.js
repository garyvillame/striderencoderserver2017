var Encoder = require('../models/encoders');
var crypto = require('crypto');
var list = [];

exports.getCheckerNames = function(req, res){

	Encoder.find({},{"EncoderID":1, "Name":1, "Login":1, "_id":0})
		.then(function(encoders) {
			if (!encoders) {
				next(new Error('No encoders.'));
			} 
			else {
				list = JSON.parse(JSON.stringify(encoders, ["EncoderID","Name","Login"]));
				res.json(list);
			}
		});
		
};


exports.addEncoder = function(req, res){
	var encoder = new Encoder(
		{
			Name: req.body.encoderData.Name,
			Login: req.body.encoderData.Login,
			Password: crypto.createHmac("sha1", "IHCSALT_")
				.update(req.body.encoderData.Password)
				.digest("hex")
		}
	);
	encoder.save(function (err, encoder) {
		if (err){ 
			return console.error("You made an error.");
		}
		res.json(encoder);
		// fluffy.speak();
	});
}