var Records = require('../models/tags')

exports.viewRecordsPerEvent = function(req, res){

	Records.find({
			"EventID": req.params.eventID
		})
		.then(function(records) {
			if (!records) {
				next(new Error('No encoders.'));
			} 
			else {
				// var list = JSON.parse(JSON.stringify(records, ["EncoderID","Name","Login"]));
				res.json(records);
			}
		});
	// res.send(req.params.eventID);

};

exports.submitEncoded = function(req, res){
	// console.log(req.body);
	// res.json(req.body);
	//for localhost
	/*var encoded = new Records(
		{
			EPC: req.body.EPC,
			EncoderTime: req.body.EncoderTime,
			PeakRSSI: req.body.PeakRSSI, 
			EncoderID: req.body.EncoderID, 
			EventID: req.body.EventID,
			Bib: req.body.Bib
		}
	);*/
	//for IH server
	var encoded = new Records(
		{
			EPC: req.body.EPC,
			Time: req.body.Time,
			RSSI: req.body.RSSI, 
			Checker: req.body.Checker, 
			EventID: req.body.EventID,
			Bib: req.body.Bib
		}
	); 
	encoded.save(function (err, encoded) {
		if (err){ 
			return console.error(err);
		}
		res.json(encoded);
		// fluffy.speak();
	});
}

exports.submitChecked = function(req, res, next){
	// console.log(req.body);
	// res.json(req.body);
	//for localhost
	/*var query = {
		EventID: req.body.EventID,
		EPC: req.body.EPC
	};
	var update = {
		CheckedTime: req.body.CheckedTime,
		PeakRSSI: req.body.PeakRSSI, 
		CheckerID: req.body.CheckerID
	}*/
	//for IH server
	var query = {
		EventID: req.body.EventID,
		EPC: req.body.EPC
	};
	var update = {
		Time: req.body.Time,
		RSSI: req.body.RSSI, 
		Checker: req.body.Checker
	}
	Records.update(query, update, function(){
		console.log("Tag checked!!!!");	
		res.send("ok");
	})

}

