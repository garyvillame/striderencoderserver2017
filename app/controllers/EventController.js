var Event = require('../models/events');


exports.getEvents = function(req, res, next){
	Event.find()
		.sort({"EventID":-1})
		.then(function(events) {
	if (!events) {
		next(new Error('No events found.'));
	  } else {
	    	res.setHeader('Content-Type', 'application/json');
	   		res.json(events);
	   	}
	});
};

exports.getEventByEventID = function(req, res, next){
	Event.findOne({"EventID":req.params.eventID})
	  .then(function(event) {
	    if (!event) {
	      next(new Error('No event with that id'));
	    } else {
	      res.json(event);
	    }
	  }, function(err) {
	    next(err);
	  });
	// res.send("EventController.getEventByEventID Success!!! :D");
}

/* Sample JSON Body for app.post('/api/addEvent')
{
	"eventData" : {
		"EventName" : "Test Event",
		"Header" : "00",
		"Pattern" : "HH00GGGG",
		"Completed" : 0,
		"TotalBibs" : 1000
	}
}
*/
exports.addEvent = function (req, res, next){
	var event = new Event();

	if( typeof req.body.eventData !== "undefined" && 
		typeof req.body.eventData.EventName !== "undefined" && 
		typeof req.body.eventData.Header !== "undefined" && 
		typeof req.body.eventData.Pattern  !== "undefined" && 
		typeof req.body.eventData.Completed  !== "undefined" && 
		typeof req.body.eventData.TotalBibs !== "undefined" ){
		event.EventName = req.body.eventData.EventName;
		event.Header = req.body.eventData.Header;
		event.Pattern = req.body.eventData.Pattern;
		event.Completed = req.body.eventData.Completed;
		event.TotalBibs = req.body.eventData.TotalBibs;

		event.save(function(err, savedEvent) {
			if(err){
				res.status(400).send("ERROR: EventName: " + event.EventName + " already exists!");
				// res.send(err);
			}
			else{
				res.send(event);
			}
		});
	}
	else{
		res.status(400).send("Posted invalid object!!!");
	}
}
	
exports.editEvent = function (req, res){
	if(typeof req.body.EventDetails !== "undefined" && typeof req.body.EventDetails.EventID !== "undefined"
	){
		var data = {};
		if(typeof req.body.EventDetails.EventID !== "undefined"){
			data.EventID = req.body.EventDetails.EventID;
		}
		if(typeof req.body.EventDetails.EventName !== "undefined"){
			data.EventName = req.body.EventDetails.EventName;
		}
		if(typeof req.body.EventDetails.Header !== "undefined"){
			data.Header = req.body.EventDetails.Header;
		}
		if(typeof req.body.EventDetails.Pattern !== "undefined"){
			data.Pattern = req.body.EventDetails.Pattern;
		}
		if(typeof req.body.EventDetails.Completed !== "undefined"){
			data.Completed = req.body.EventDetails.Completed;
		}
		if(typeof req.body.EventDetails.TotalBibs !== "undefined"){
			data.TotalBibs = req.body.EventDetails.TotalBibs;
		}

		Event.update({ EventID: req.body.EventDetails.EventID}, 
			{ $set: data}, 
			function(err){
				if(err){
					res.status(400).send("ERROR: Update failed.");
				}
				else{
					res.status(200).send("SUCCESS: Update completed.");
				}
			}
		);
	}
	else{
		res.status(400).send("ERROR: Posted invalid object!!! Update failed.");
	}
}

