var User = require('../models/encoders');

var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;

passport.use(new LocalStrategy(
  function(username, password, done) {
    User.findOne({ username: username }, function (err, user) {
      if (err) { return done(err); }
      if (!user) {
        return done(null, false, { message: 'Incorrect username.' });
      }
      if (!user.validPassword(password)) {
        return done(null, false, { message: 'Incorrect password.' });
      }
      return done(null, user);
    });
  }
));

passport.use(new LocalStrategy(User.authenticate()))
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());
// exports.userLogin = function(req, res){
// 	passport.use(new LocalStrategy(
// 		function(username, password, done) {
// 			User.findOne({ Name: "Roman" }, function(err, user) {
// 				if (err) { 
// 					return done(err); 
// 				}
// 				if (!user) {
// 					console.log("hola datevid");
// 					return done(null, false, { message: 'Incorrect username.' });
// 				}
// 				// if (!user.validPassword(password)) {
// 				// 	return done(null, false, { message: 'Incorrect password.' });
// 				// }
// 				res.send(user);
// 			});
// 		}
// 	));
// }