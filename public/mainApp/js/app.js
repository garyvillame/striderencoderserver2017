'use strict';

// Note: 'strider-encoder-server' is similar to the app declared in layout.html (see line below)
// <html lang="en" ng-app="strider-encoder-server" ng-controller="appCtrl">


angular.module('strider-encoder-server', ['strider-encoder-server.controllers.appCtrl', 'ngRoute'
])

.config(['$routeProvider', '$locationProvider',
function ($routeProvider, $locationProvider) {

  $routeProvider

    /*
     * FRONT-END
     */

    // Index
    .when('/', {
      templateUrl: '/mainApp/partials/index.html',
      controller: 'appCtrl'
    })

    // Manage Events
    .when('/manageEvents', {
      templateUrl: '/mainApp/partials/index.html',
      controller: 'appCtrl'
    })

    // Others
    .when('/pageNotFound', {
      templateUrl: '/mainApp/partials/pageNotFound.html'
    })

    // Catch-all
    .otherwise({
      redirectTo: '/pageNotFound' // temporarily point to pageNotFound while no index page design is available/implemented (otherwise, redirect to '/')
    });

  $locationProvider
    .html5Mode(false);

}])

// Application-wide settings
.constant('apiUrl', '')

.config(['$interpolateProvider',
function($interpolateProvider){

  $interpolateProvider.startSymbol('[[');
  $interpolateProvider.endSymbol(']]');
}]);